---
title: "Soveltavan data-analyysin perusteet R-kielellä - SOSI1225 A.5 (3 op)"
output:
  html_document:
    toc: true
    toc_float: true
#    number_sections: yes
---

`Sivusto päivitetty: `r Sys.time()``

<!-- <div class="alert alert-dismissible alert-danger"> -->
<!--   <button type="button" class="close" data-dismiss="alert">&times;</button> -->
<!--   <strong>Huomio!!</strong> </br>Kurssi vasta tekeillä, vaikka ilmoittautua toki jo voi! </br> Kaikki sisältö ainoastaan testausta!! -->
<!-- </div> -->

# Uutta sivuilla:


- `2016-10-30`: Neljännen luennon kotitehtävien osa II: <http://courses.markuskainu.fi/utur2016/session4_visualise_model_kotitehtava_osa2.html>
- `2016-10-28`: Neljännen luennon kotitehtävien osa I: <http://courses.markuskainu.fi/utur2016/session4_visualise_model_kotitehtava.html>
- `2016-10-28`: Neljäs luento: <http://courses.markuskainu.fi/utur2016/session4_visualise_model.html>
- `2016-10-28`: Kolmannen kotitehtävän vastaukset: <http://courses.markuskainu.fi/utur2016/session3_transform_kotitehtava_valmis.html>
- `2016-10-21`: Kolmas kotitehtävä julkaistu: <http://courses.markuskainu.fi/utur2016/session3_transform_kotitehtava.html>
- `2016-10-21`: Toisen kotitehtävän vastaukset: <http://courses.markuskainu.fi/utur2016/session2_import_tidy_kotitehtava_valmis.html>
- `2016-10-14`: Päivitetyt ohjeet kotitehtävien palauttamiselle <http://courses.markuskainu.fi/utur2016/kotitehtavat.html>


# Yleiskuvaus

Kurssilla perehdytän laskennallisen data-analyysin perusasioihin R-ohjelmistolla. Kurssilla käydään läpi data-analyysin prosessi eri datalähteistä, datan siivoamisen ja muokkaamisen kautta visualisoimiseen ja mallintamiseen, sekä analyysin tulosten raportoimiseen. Kurssilla käytetään ensisijaisesti R-ohjelmiston suosituimpia paketteja kuten tidyr, dplyr, ggplot2 tai knitr. R-kielen perusteita kurssilla käsitellään vain kursorisesti. Kurssin tavoitteena on antaa perustiedot ja taidot laskennallisen data-analyysin toteuttamisesta R-kielen nykyaikaisilla työkaluilla ja motivoida omatoimiseen harjoitteluun. Hyvää tukea kurssille antaa kurssi [TILM3517 R-kielen alkeet 2 op](https://nettiopsu.utu.fi/opas/opetusohjelma/marjapuuro.htm?id=13594)

Mikäli R on sinulle aivan vieras suosittelen tutustumaan perusteisiin esimerkiksi [tryR](http://tryr.codeschool.com/):n tai [DataCamp](https://www.datacamp.com/courses/free-introduction-to-r):n kurssien avulla.

# Kurssille ilmoittautuminen

<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Kurssille ilmoittautuminen on päättynyt!</strong>
</div>


<div class="alert alert-dismissible alert-warning">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Huomio!</h4>
  <p>Tee seuraavat kuusi (6) asiaa ENNEN kurssin alkua!</p>
</div>

Käytä tunnuksena kaikissa eri rekisteröitymisen vaiheissa lyhyttä `tunnus`@utu.fi tunnustasi, kuten `mjkain`. (viittaan ohjeissa ko. tunnukseen `utu_tunnus`). Mikäli se tunnus on varattu, lisää sen perään numero `11`.

1. Luo tili [Gitlab](https://gitlab.com/users/sign_in)-koodinjakopalveluun ja **käytä `utu_tunnus`:ta ja paina salasana mieleesi**
2. Täytä [ilmoittautumislomake](http://muuankarski.org/limesurvey/index.php/129581?lang=fi)
3. Kirjaudu Gitlab-tunnuksillasi [Gitlab](https://gitlab.com/users/sign_in):iin
4. Mene osoitteeseen <https://gitlab.com/groups/utur2016> (Sinun tulee olla kirjautuneena palveluun tällä selaimella!) ja klikkaa **Request access** ([ks. kuva](http://courses.markuskainu.fi/utur2016/kuviot/request_access.jpg))
5. Muutamaa tuntia myöhemmin saat ilmoittamaasi sähköpostiisi kutsun Slack-ryhmään <https://utur2016.slack.com/>. Klikkaa sähköpostin linkkiä ja rekisteröidy palveluun **käyttäen `utu_tunnus`:ta ja paina salasana mieleesi**
6. Ennen kurssin alkua tutustu:
    - [ohjelmistoympäristön asentamiseen](ohjelmistoymparisto.html)
    - [ensimmäisen kerran materiaaleihin](session1_intro.html)


# Tiedot

- **Opettaja:** [Markus Kainu](http://markuskainu.fi/)
- **Sähköposti:** `markuskainu@gmail.com` (ks. [sähköpostipolitiikka](ukk.html#sahkopostipolitiikka))
- **Koodi:** SOSI1225 A.5 - [kurssi nettiopsussa](https://nettiopsu.utu.fi/opas/opetusohjelma/marjapuuro.htm?id=13887)
- **Laajuus:** 3op
- **Ajankohta:** Lokakuun 7. - marraskuun 4. 2016
- **Luentojen aika:** Perjantaina klo 10-12
- **Luentosali:** Publicumin 4. kerroksen mikroluokka 408


# Aikataulu

| Päivä            | Aika  | Paikka        | Materiaalit  | Kotitehtävät     |
| ---------          | ----  | ------       | -----------------------         | ----------    | 
| perjantai, 7.10  | 10-12 | Publicumin 4. kerroksen mikroluokka 408  | [luento](./session1_intro.html)         | [kotitehtava.R](https://gitlab.com/utur2016/content/raw/master/session1_intro_kotitehtava.R) |
| perjantai, 14.10 | 10-12 | Publicumin 4. kerroksen mikroluokka 408 | [luento](./session2_import_tidy.html)             | [kotitehtava.R](https://gitlab.com/utur2016/content/raw/master/session2_import_tidy_kotitehtava.R) |
| perjantai, 21.10 | 10-12 | Publicumin 4. kerroksen mikroluokka 408 | [luento](./session3_transform.html)                          | [kotitehtava.R](https://gitlab.com/utur2016/content/raw/master/session3_transform_kotitehtava.R) |
| perjantai, 28.10 | 10-12 | Publicumin 4. kerroksen mikroluokka 408 | [luento](./session4_visualise_model.html)| [kotitehtava.R](https://gitlab.com/utur2016/content/raw/master/session4_visualise_model_kotitehtava.R) |
| perjantai, 4.10  | 10-12 | Publicumin 4. kerroksen mikroluokka 408 | [luento](./session5_communicate.html)                      | [kotitehtava.R](https://gitlab.com/utur2016/content/raw/master/session5_communicate_kotitehtava.R) |

- kalenteri [Googlen kalenterissa](https://calendar.google.com/calendar/embed?src=rbarcmn1t5ci5endg3phndkc5c%40group.calendar.google.com&ctz=Europe/Helsinki)

<!--
<!--html_preserve-->
<iframe src="https://calendar.google.com/calendar/embed?src=rbarcmn1t5ci5endg3phndkc5c%40group.calendar.google.com&ctz=Europe/Helsinki" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
<!--/html_preserve-->
-->



# Kotitehtävät

ks. ylänavigaatiosta kohdasta "Kotitehtävät"


<!-- | Luennon pvm | DL |linkki kotitehtäviin -->
<!-- | ---    | ---    | ------------------------------  | -->
<!-- | 7.10.  | 14.10. | [1. kotitehtava](https://gitlab.com/utur2016/content/blob/master/session1_intro_kotitehtava.R) | -->
<!-- | 14.10. | 21.10. | [2. kotitehtava](https://gitlab.com/utur2016/content/blob/master/session2_import_tidy_kotitehtava.R) | -->
<!-- | 21.10. | 28.10. | [3. kotitehtava](https://gitlab.com/utur2016/content/blob/master/session3_transform_kotitehtava.R) | -->
<!-- | 28.10. |  4.10. | [4. kotitehtava](https://gitlab.com/utur2016/content/blob/master/session4_visualise_model_kotitehtava.R) | -->
<!-- | 4.10.  | 11.10. | [5. kotitehtava](https://gitlab.com/utur2016/content/blob/master/session5_communicate_kotitehtava.R) | -->



# Lähdekoodi

Ajantasainen lähdekoodin löytyy [Gitlab](https://gitlab.com/)-koodinjakopalvelusta polusta: 

- verkkosivu: <https://gitlab.com/utur2016/website>
- sisältö: <https://gitlab.com/utur2016/content>
- tutkimuspaperin kirjoittaminen: <https://gitlab.com/utur2016/paper>
- uuden tutkimusprojektin aloittaminen: <https://gitlab.com/utur2016/blank>
