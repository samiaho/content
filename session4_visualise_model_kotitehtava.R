#' ---
#' title: "Kotitehtävä 4"
#' author: utu_tunnus
#' output:
#'   html_document:
#'  #   toc: true
#'  #   toc_float: true
#'     number_sections: yes
#'     code_folding: show
#' ---


#' [Linkki kotitehtävän lähdekoodiin gitlab:ssa](https://gitlab.com/utur2016/content/raw/master/session4_visualise_model_kotitehtava.R)




#+ setup
install.packages("tidyverse")
install.packages("ggplot2")
install.packages("knitr")
library(knitr)
library(ggplot2)
opts_chunk$set(list(echo=TRUE,eval=FALSE,cache=FALSE,warning=TRUE,message=TRUE))
library(tidyverse)


#' ## Kansiorakenteen luominen
#'
#' **Millä komennolla luot nykyisen työhakemistoon kansion `kotitehtava4`?**
#+ vastaus1
dir.create("kotitehtava4")


#' **Millä komennolla luot kansion 'kotitehtava4` alle kansion `kuviot`?**
#+ vastaus2
dir.create(".//kotitehtava4/kuviot")


#'
#'
#' Datojen hakeminen
#'
#' Käytämme nyt taas dataa FAO:sla, joka löytyy osoiteesta:
#' '<http://faostat3.fao.org/faostat-bulkdownloads/Production_Crops_E_All_Data_(Norm).zip>
#' 
#' Data on valmiiksi prosessoitu seuraavalla skriptillä:
#' 
#'     download.file("http://faostat3.fao.org/faostat-bulkdownloads/Production_Crops_E_All_Data_(Norm).zip",
#'     destfile="../website/database/Production_Crops_E_All_Data_(Norm).zip")
#'     unzip("./Production_Crops_E_All_Data_(Norm).zip")
#'     d <- readr::read_csv("../website/database/Production_Crops_E_All_Data_(Norm).csv")
#'     saveRDS(d, "../website/database/Production_Crops_E_All_Data_(Norm).RDS")
#'     
#'  Data on ladattavissa R:ään komennolla `fao <- readRDS(gzcon(url("http://courses.markuskainu.fi/utur2016/database/Production_Crops_E_All_Data_(Norm).RDS")))`
#'
#'  Lataa data ja printtaa montako erillistä maata datassa on
#+ vastaus3
fao <- readRDS(gzcon(url("http://courses.markuskainu.fi/utur2016/database/Production_Crops_E_All_Data_(Norm).RDS")))
unique(fao$Country)


#' *Jatkossa tehväsissä oletetaan että objekti `fao`**
#' 
#' 
#' **Kuinka monta tonnia Suomessa tuotettiin tomaatteja vuonna 1990-2012**?
#' **Tee uudesta datasta objekti `tomatoes`*
#+ vastaus4
unique(fao$Year)
unique(fao)
unique(fao$Element)
unique(fao$Item)
tomatoes <- fao %>%
  filter(Country == "Finland", Year >= 1990, Year <=2012, Item == "Tomatoes", Element == "Production") %>%
  select(Country, Year, Item, Value)


#' Luennolla käytiin läpi että ggplot2-paketilla piirretään viivakuvio syntaksiilla
#' `ggplot(data=d, aes(x=xmuuttuja,y=ymuuttuja)) + geom_line()`
#' 
#' **Piirrä datasta `tomatoes` viivakuvio jossa x-akselilla on vuodet ja 
#' y-akselilla tomaattien määrä**
#'
#+ vastaus5
ggplot(data=tomatoes, aes(x = Year, y = Value, colour = Item)) + geom_line()



#' Luo aikaisemman tehtävän tapaan data ja ota siihen tomaattien ohella myös 
#' ruis, vehnä ja kaura ja anna nimeksi `viljat`. 
#' 
#' Piirrä datasta samanmoinen viivakuvio, 
#' jossa eri viljat on eri värisellä viivalla
#' 
#+ vastaus6
viljat <- fao %>%
  filter(Country == "Finland", Year >= 1990, Year <=2012, Item == "Tomatoes" | Item == "Oats" | Item == "Wheat" | Item == "Rye", Element == "Production") %>%
  select(Country, Year, Item, Value, Element)
ggplot(data=viljat, aes(x = Year, y = Value, colour=Item)) + geom_line()


#' Tee samoista viljoista toinen kuvio, jossa tuotannon sijaan tarkastellaan
#' viljelyalaa ja että yksikkö on tonnien sijaan hehtaari.
#' 
#+ vastaus7

viljatha <- fao %>%
  filter(Country == "Finland", Year >= 1990, Year <=2012, Item == "Tomatoes" | Item == "Oats" | Item == "Wheat" | Item == "Rye", Element == "Area harvested") %>%
  select(Country, Year, Item, Value, Element)

ggplot(data=viljatha, aes(x = Year, y = Value, colour =Item)) + geom_line()





#' Tehdään sitten tolppakuvioita. Valitse edellisen esimerkin kasvit ja viljelyala, 
#' mutta valitse vuodeksi 2012. Piirrä tolppakuvio geom_bar()-funktiolla 
#' (muista `stat="identity"` parametri!
#+ vastaus8
viljat2012 <- fao %>%
  filter(Country == "Finland", Year ==2012, Item == "Tomatoes" | Item == "Oats" | Item == "Wheat" | Item == "Rye", Element == "Area harvested") %>%
  select(Country, Year, Item, Value, Element)

ggplot(data=viljat2012, aes(x = Item, y = Value, fill = Item )) + geom_bar(stat = "identity")


#' Pudota tomaatit ja ruis pois tarkastelusta ja tarkastellaan sadon määrää (Yield) yksikkönä
#' hehtogrammaa hehtaarilta. Ota mukaan myös maat joilla on Suomen kanssa maaraja ja piirrä sama tolppakuvio mutta siten että maat ovat omia paneelejaan (`facet_wrap(~var)`)
#' 
#+ vastaus9
viljatnaap <- fao %>%
  filter(Element == "Yield", Country == "Finland" | Country == "Russian Federation" | Country == "Sweden" | Country == "Norway", Year == 2012, Item == "Oats" | Item == "Wheat") %>%
  select(Country, Year, Item,Element, Unit, Value)

viljanaaplot <- ggplot(data=viljatnaap, aes(x = Item, y = Value, fill = Item)) + geom_bar(stat = "identity")
viljanaaplot + facet_wrap(~Country)


#' Pudotetaan Venäjä pois ja tarkastellaan Suomen, Ruotsin, Norjan ja Tanskan
#' kesken vehnän tuotannon osalta sitä, miten 
#' tuotannon määrä (tonneja) ja viljelysala ovat yhteydessä toisiinsa piirtämällä hajontakuvio
#' jossa x-akselilla on tuotanto ja y-akselilla viljelysala. Kenties helpoin tapa on tehdä kaksi dataa
#' ja yhdistää ne left join. Saatat joutua vaihtamaan Value-muuttujan nimeä.
#+ vastaus10
viljatala <- fao %>%
  filter(Element == "Area harvested", Country == "Finland" | Country == "Denmark" | Country == "Sweden" | Country == "Norway", Year == 2012, Item == "Oats" | Item == "Wheat") %>%
  select(Country, Year, Item, Element, Unit, Value) %>%
  mutate(Hehtaarit = Value) %>%
  select(Country, Year, Item, Hehtaarit)

viljattonni <- fao %>%
  filter(Element == "Production", Country == "Finland" | Country == "Denmark" | Country == "Sweden" | Country == "Norway", Year == 2012, Item == "Oats" | Item == "Wheat") %>%
  select(Country, Year, Item, Element, Unit, Value) %>%
  mutate(Tonnit = Value) %>%
  select(Country, Year, Item, Tonnit)

alatonni <- left_join(viljatala,viljattonni)

alatonniplot <- ggplot(alatonni, aes(x=Tonnit, y=Hehtaarit, colour = Item)) +
  geom_point(shape=1)
alatonniplot
alatonniplot + facet_wrap(~Country)


#' Jatketaan samoilla spekseillä mutta otetaan vuodet 1990-2012 ja piirretään maittaiset viivakuviot
#' niin että paneeleissa on tuotanto ja viljelysala 
#' 
#+ vastaus11
viljatala2 <- fao %>%
  filter(Year >= 1990, Year <= 2012, Element == "Area harvested", Country == "Finland" | Country == "Denmark" | Country == "Sweden" | Country == "Norway",  Item == "Oats" | Item == "Wheat") %>%
  select(Country, Year, Item, Element, Unit, Value)%>%
  mutate(Hehtaarit = Value) %>%
  select(Country, Year, Item, Hehtaarit)

viljattonni2 <- fao %>%
  filter(Year >= 1990, Year <= 2012, Element == "Production", Country == "Finland" | Country == "Denmark" | Country == "Sweden" | Country == "Norway", Item == "Oats" | Item == "Wheat") %>%
  select(Country, Year, Item, Element, Unit, Value) %>%
  mutate(Tonnit = Value) %>%
  select(Country, Year, Item, Tonnit)

alatonni2 <- left_join(viljatala2,viljattonni2)

alatonniplot2 <- ggplot(alatonni2, aes(Year, Hehtaarit)) + geom_line(aes(y = Hehtaarit, colour = "Hehtaarit")) + geom_line(aes(y = Tonnit, colour = "Tonnit"))
alatonniplot2
alatonniplot2 + facet_wrap(~alatonni$Hehtaarit)
